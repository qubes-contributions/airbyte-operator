# airbyte-operator
### Apply the CRDs
```
kubectl apply -f crds/
```

### Install the dependencies
```
pip install kopf
```

### Edit endpoint and start the kopf for our Airbyte CRDs
```
vim kopf_airbytes.py 
kopf run kopf_airbytes.py 
```

### Run the charts:
```
# Edit the corresponding connections
vim charts/values.yaml
# Start our helm charts
helm upgrade -i airbyte-components ./charts/ -f charts/values.yaml
```

### Verify on Airbyte UI
