from functools import lru_cache

from decouple import config
from pydantic import BaseSettings


class AppSettings(BaseSettings):
    """
    Settings for the FastAPI App.
    All values are read from the .env file which is stored within
    the app folder.
    python-decouple's config method is used to extract the values
    and assign them to the constant within the AppSettings Class.
    """
    LOCAL_URL = config("LOCAL_URL", default="https://localhost:8000")
    AIRBYTE_API_URL = config("AIRBYTE_API_URL", default="http://airbyte-airbyte-server-svc:8001")
    AIRBYTE_API_VERSION = config("AIRBYTE_API_VERSION", default="v1")
    DELAY_RETRIES = config("DELAY_RETRIES", default=10.0)
    RETRIES = config("RETRIES", default=10)
    SYNC_INTERVAL = config("SYNC_INTERVAL", default=60.0)
    SYNC_IDLE = config("SYNC_IDLE", default=15.0)

class Settings(
    AppSettings,
):
    """
    An abstract class which inherits from other classes
    """


@lru_cache()
def get_app_config():
    """
    Returns a Settings Class
    """

    return Settings()