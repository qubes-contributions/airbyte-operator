import kopf
from logzero import logger
from services.airbyte_source import create_airbyte_source, update_airbyte_source, delete_airbyte_source, list_airbyte_source
from services.airbyte_destination import create_airbyte_destination, update_airbyte_destination, delete_airbyte_destination, list_airbyte_destination
from services.airbyte_connection import create_airbyte_connection, delete_airbyte_connection, trigger_airbyte_job, list_airbyte_connection
from services.airbyte_common import get_id_by_name
from services.airbyte_workspace import create_workspace

from config.config import get_app_config
settings = get_app_config()

AIRBYTE_BASEURL = f"{settings.AIRBYTE_API_URL}/v1"
RETRIES = int(settings.RETRIES)
DELAY_RETRIES = float(settings.DELAY_RETRIES)
SYNC_INTERVAL = float(settings.SYNC_INTERVAL)
SYNC_IDLE = float(settings.SYNC_IDLE)

@kopf.on.startup()
def create_ab_workspace(**kwargs):

    workspace_id = ""

    call_create_workspace = create_workspace()

    if call_create_workspace["status"] == "ok":
        workspace_id = call_create_workspace["workspaceId"]
    else:
        print("Error")

    return_data = {
        "message": call_create_workspace.get("message")
    }

    return return_data



@kopf.on.create('airbytesources')
def create_absource(spec, meta, patch, **kwargs):
    meta_data = dict(meta)
    spec_data = dict(spec)

    call_airbyte_api = create_airbyte_source(meta_data, spec_data)

    if call_airbyte_api['status'] == 'ok': 
        patch.spec['source_id'] = call_airbyte_api.get('uuid')
        patch.setdefault('status', {}).setdefault('source', {})['workspaceId'] = call_airbyte_api.get('workspace_id')
    else:
        ## handling error
        print('err')
 
    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.update('airbytesources')
def update_absource(spec, **kwargs):
    spec_data = dict(spec)

    call_airbyte_api = update_airbyte_source(spec_data)

    if call_airbyte_api['status'] == 'ok':
        print("Ok")
    else:
        ## handling error
        logger.error(call_airbyte_api['message'])

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status


@kopf.on.delete('airbytesources')
def delete_absource(spec, **kwargs):
    spec_data = dict(spec)
    call_airbyte_api = delete_airbyte_source(spec_data)

    if call_airbyte_api['status'] == 'ok':
        print('success')
    else:
        ## handling error
        print('err')

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.create('airbytedestinations')
def create_abdestination(spec, meta, patch, **kwargs):
    meta_data = dict(meta)
    spec_data = dict(spec)

    call_airbyte_api = create_airbyte_destination(meta_data, spec_data)

    if call_airbyte_api['status'] == 'ok':
        patch.spec['destination_id'] = call_airbyte_api.get('uuid')
        patch.setdefault('status', {}).setdefault('destination', {})['workspaceId'] = call_airbyte_api.get('workspace_id')
    else:
        ## handling error
        print('err')

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.update('airbytedestinations')
def update_abdestination(spec, **kwargs):
    spec_data = dict(spec)
    
    call_airbyte_api = update_airbyte_destination(spec_data)

    if call_airbyte_api['status'] == 'ok':
        print("Ok")
    else:
        ## handling error
        logger.error(call_airbyte_api['message'])

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.delete('airbytedestinations')
def delete_abdestination(spec, **kwargs):
    spec_data = dict(spec)

    call_airbyte_api = delete_airbyte_destination(spec_data)

    if call_airbyte_api['status'] == 'ok':
        print('success')
    else:
        ## handling error
        print('err')

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.create('airbyteconnections')
def create_abconnection(spec, meta, patch, retry, **kwargs):
    meta_data = dict(meta)
    spec_data = dict(spec)

    source_id = ""
    destination_id = ""
    call_airbyte_api = {
        'status': None
    }
    ### Handle case without the id, only names
    # Fetch sourceId, if empty, based on sourceName
    if not spec.get('connection', {}).get('sourceId'):
        source_id = get_id_by_name(spec.get('connection', {}).get('sourceName'), 'sources')
        
        # Handling case on creatation that the source/dest doesn't exist yet
        if not source_id and retry < RETRIES:
            raise kopf.TemporaryError("The src_id not found.", delay=DELAY_RETRIES)
        
        print(f"Patching sourceId {source_id}")
        patch.setdefault('status', {}).setdefault('connection', {})['sourceId'] = source_id
        spec_data['connection']['sourceId'] = source_id

    # Fetch sourceId, if empty, based on sourceName
    if not spec.get('connection', {}).get('destinationId'):
        destination_id = get_id_by_name(spec.get('connection', {}).get('destinationName'), 'destinations')
        
        # Handling case on creatation that the source/dest doesn't exist yet
        if not destination_id and retry < RETRIES:
            raise kopf.TemporaryError("The dest_id not found.", delay=DELAY_RETRIES)
        
        print(f"Patching destinationId {destination_id}")
        patch.setdefault('status', {}).setdefault('connection', {})['destinationId'] = destination_id
        spec_data['connection']['destinationId'] = destination_id



    print(f"After patch {spec_data}")
    print(f'src - dest: {source_id} - {destination_id}')
    if source_id and destination_id:
        call_airbyte_api = create_airbyte_connection(meta_data, spec_data)
    return_data = {}
    if call_airbyte_api['status'] == 'ok':
        # Call trigger job first time
        connection_id = call_airbyte_api.get('uuid')
        patch.spec['connection_id'] = connection_id
        trigger_airbyte_job(connection_id, 'sync')
        patch.setdefault('status', {}).setdefault('connection', {})['workspaceId'] = call_airbyte_api.get('workspace_id')
    else:
        ## handling error
        print('err')

    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status

@kopf.on.update('airbyteconnections')
def update_abconnection(spec, meta, patch, **kwargs):
    meta_data = dict(meta)
    spec_data = dict(spec)

    ### Handle case without the id, only names
    data_id = get_id_by_name(spec.get('connection', {}).get('sourceName'), 'sources')
    if data_id:
        print(f"Patching sourceId {data_id}")
        patch.setdefault('status', {}).setdefault('connection', {})['sourceId'] = data_id
        spec_data['connection']['destinationId'] = data_id
    else:
        return "error getting source_id"

    data_id = get_id_by_name(spec.get('connection', {}).get('destinationName'), 'destinations')
    if data_id:
        print(f"Patching destinationId {data_id}")
        patch.setdefault('status', {}).setdefault('connection', {})['destinationId'] = data_id
        spec_data['connection']['destinationId'] = data_id
    else:
        return "error getting destination_id"

    print(f"After patch {spec_data}")

    return_data = "oke"
    return return_data  # will be the new status

@kopf.on.delete('airbyteconnections')
def delete_abconnection(spec, **kwargs):
    spec_data = dict(spec)
    call_airbyte_api = delete_airbyte_connection(spec_data)
    
    if call_airbyte_api['status'] == 'ok': 
        print('success')
    else:
        ## handling error
        print('err')
        
    return_data = {
        'message': call_airbyte_api.get('message'),
    }
    return return_data  # will be the new status


@kopf.timer('airbytesources', interval=SYNC_INTERVAL, idle=SYNC_IDLE)
def sync_absource(spec, meta, patch, **kwargs):
    ## keep checking
    meta_data = dict(meta)

    source_name = meta_data['name']
    source_list = list_airbyte_source()

    is_existed = False
    for source in source_list:
        if source_name == source['name']:
            is_existed = True
            # update the current one
            logger.info(f"+++ update source: {source_name}")
            update_absource(spec, **kwargs)
            return

    if is_existed == False:
        # create if not exist
        logger.info(f"+++ create source: {source_name}")
        create_absource(spec, meta, patch, **kwargs)


@kopf.timer('airbytedestinations', interval=SYNC_INTERVAL, idle=SYNC_IDLE)
def sync_abdestination(spec, meta, patch, **kwargs):
    ## keep checking
    meta_data = dict(meta)

    destination_name = meta_data['name']
    destination_list = list_airbyte_destination()

    is_existed = False
    for dest in destination_list:
        if destination_name == dest['name']:
            is_existed = True
            # update the current one
            logger.info(f"+++ update {destination_name}")
            update_abdestination(spec, **kwargs)
            return

    if is_existed == False:
        # create if not exist
        logger.info(f"+++ create {destination_name}")
        create_abdestination(spec, meta, patch, **kwargs)

@kopf.timer('airbyteconnections', interval=SYNC_INTERVAL, idle=SYNC_IDLE)
def sync_connection(spec, meta, patch, retry, **kwargs):
    ## keep checking
    meta_data = dict(meta)

    connection_name = meta_data['name']
    connection_list = list_airbyte_connection()

    is_existed = False
    for conn in connection_list:
        if connection_name == conn['name']:
            is_existed = True
            # update the current one
            logger.info(f"+++ update connection: {connection_name}")
            update_abconnection(spec, meta, patch, **kwargs)
            return

    if is_existed == False:
        # create if not exist
        logger.info(f"+++ create connection: {connection_name}")
        create_abconnection(spec, meta, patch, retry, **kwargs)
