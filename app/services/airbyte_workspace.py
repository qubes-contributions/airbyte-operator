import requests
from logzero import logger

from config.config import get_app_config
settings = get_app_config()

AIRBYTE_BASEURL = f"{settings.AIRBYTE_API_URL}/{settings.AIRBYTE_API_VERSION}"


def create_workspace():
    url = f"{AIRBYTE_BASEURL}/workspaces"

    payload = { "name": "workspace" }
    headers = {
        "accept": "application/json",
        "content-type": "application/json"
    }

    logger.debug(f'+++ Creating workspace...')
    response = requests.post(url, json=payload, headers=headers)
    logger.debug(f'+++: {response.status_code} - {response.text}')

    if response.status_code == 200:  # successful
        response_data = response.json()
        return_value = {
            "workspaceId": response_data["workspaceId"],
            "name": response_data["name"],
            "status": "ok",
            "message": "Create workspace successful"
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value