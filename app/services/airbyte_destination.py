import requests
from logzero import logger

from config.config import get_app_config
settings = get_app_config()

from services.airbyte_common import get_id_by_name

AIRBYTE_BASEURL = f"{settings.AIRBYTE_API_URL}/{settings.AIRBYTE_API_VERSION}"
airbyte_url = AIRBYTE_BASEURL

def create_airbyte_destination(meta: dict, spec: dict) -> dict:
    """_summary_

    Args:
        meta (dict): metadata for the object
        spec (dict): spec of the ofject

    Returns:
        dict: {
            "status": "ok"
            "uuid": "XXX"
            "message": "some description"
        }
    """
    # Construct the API URL
    url = f"{airbyte_url}/destinations"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}

    # Wrap the request body
    if spec['destination']:
        dest_type = next(iter(spec['destination']))
    else:
        return {
            'status': 'bad',
            'message': 'empty dict'
        }

    spec['destination'][dest_type].update({'destinationType': dest_type})

    name = meta['name']
    configuration = spec.get('destination').get(dest_type)
    workspace_id = get_id_by_name('workspaceId', 'workspaces')

    json_payload = {
        'configuration': configuration,
        'name': name,
        'workspaceId': workspace_id
    }

    # Make the POST request
    logger.debug(f'+++: POST {url} \n Param {params_data} \n Header {header_data} \n Payload {json_payload}')
    response = requests.post(url, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 200: # successful
        response_data = response.json()
        return_value = {
            'status': 'ok',
            'uuid': response_data.get('destinationId'),
            'workspace_id': response_data.get('workspaceId'),
            'message': 'create successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value

def update_airbyte_destination(spec: dict) -> dict:
    # Construct the API URL
    dest_id = spec.get('destination_id')
    url = f"{airbyte_url}/destinations/{dest_id}"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}

    # Wrap the request body
    # get the type
    dest_type = next(iter(spec['destination']))
    configuration = spec.get('destination').get(dest_type)

    json_payload = {
        'configuration': configuration
    }

    # Make the DELETE request
    logger.debug(f'+++: PATCH {url} \n Param {params_data} \n Header {header_data} \n Payload {json_payload}')
    response = requests.patch(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')

    if response.status_code == 200: # successful
        #response_data = response.json()
        return_value = {
            'status': 'ok',
            'message': 'delete successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value

def delete_airbyte_destination(spec: dict) -> dict:
    """_summary_

    Args:
        meta (dict): metadata for the object
        spec (dict): spec of the ofject

    Returns:
        dict: {
            "status": "ok"
            "uuid": "XXX"
            "message": "some description"
        }
    """    
    # Construct the API URL
    dest_id = spec.get('destination_id')
    url = f"{airbyte_url}/destinations/{dest_id}"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}
    
    # Wrap the request body
    json_payload = {}

    # Make the DELETE request
    logger.debug(f'+++: DELETE {url} \n Param {params_data} \n Header {header_data} \n Payload {json_payload}')
    response = requests.delete(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 204: # successful
        #response_data = response.json()
        return_value = {
            'status': 'ok',
            'message': 'update successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value

def list_airbyte_destination() -> list:
    """_summary_

    Returns:
        list: list of destination
    """
    # Construct the API URL
    url = f"{airbyte_url}/destinations"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}

    # Wrap the request body
    json_payload = {}

    # Make the DELETE request
    logger.debug(f'+++: GET {url} \n Param {params_data} \n Header {header_data} \n Payload {json_payload}')
    response = requests.get(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 200: # successful
        response_data = response.json()
        # handle empty list
        if 'data' in response_data:
            return_value = response_data['data']
            return return_value
    else:
        logger.error(f'{response.status_code} - {response.text}')
    
    return_value = []
    return return_value
