import requests
from logzero import logger

from config.config import get_app_config
settings = get_app_config()

from services.airbyte_common import get_all_stream

AIRBYTE_BASEURL = f"{settings.AIRBYTE_API_URL}/{settings.AIRBYTE_API_VERSION}"
airbyte_url = AIRBYTE_BASEURL

def create_airbyte_connection(meta: dict, spec: dict) -> dict:
    """_summary_

    Args:
        meta (dict): metadata for the object
        spec (dict): spec of the ofject

    Returns:
        dict: {
            "status": "ok"
            "uuid": "XXX"
            "message": "some description"
        }
    """    
    # Construct the API URL
    url = f"{airbyte_url}/connections"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request body
    payload = spec['connection']
    payload['name'] = meta['name']
    source_id = payload['sourceId']
    destination_id = payload['destinationId']
    # Return list of streams in configuration
    call_get_stream = get_all_stream(source_id, destination_id)

    if call_get_stream['status'] == 'ok':
        payload['configurations'] = call_get_stream['data']

    json_payload = payload

    # Make the POST request
    logger.debug(f'+++: Calling {url} - Header {header_data} - Payload {json_payload}')
    response = requests.post(url, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 200: # successful
        response_data = response.json()
        return_value = {
            'status': 'ok',
            'uuid': response_data.get('connectionId'),
            'workspace_id': response_data.get('workspaceId'),
            'message': 'create successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }
        
    return return_value

def delete_airbyte_connection(spec: dict) -> dict:
    """_summary_

    Args:
        meta (dict): metadata for the object
        spec (dict): spec of the ofject

    Returns:
        dict: {
            "status": "ok"
            "uuid": "XXX"
            "message": "some description"
        }
    """    
    # Construct the API URL
    logger.debug(f"Here we go: {spec}")
    connection_id = spec.get('connection_id')
    url = f"{airbyte_url}/connections/{connection_id}"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}
    
    # Wrap the request body
    json_payload = {}

    # Make the DELETE request
    logger.debug(f'+++: Calling {url} - Header {header_data} - Payload {json_payload}')
    response = requests.delete(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 204: # successful
        #response_data = response.json()
        return_value = {
            'status': 'ok',
            'message': 'delete successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value

def trigger_airbyte_job(connection_id: str, job_type='sync') -> dict:
    # Construct the API URL
    url = f"{airbyte_url}/jobs"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}
    
    # Wrap the request body
    json_payload = {
        'connectionId': connection_id,
        'jobType': job_type
    }

    # Make the POST request
    logger.debug(f'+++: Calling {url} - Header {header_data} - Payload {json_payload}')
    response = requests.post(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 200: # successful
        #response_data = response.json()
        return_value = {
            'status': 'ok',
            'message': 'job create successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }

    return return_value


def list_airbyte_connection() -> list:
    """_summary_

    Returns:
        list: list of sources
    """
    # Construct the API URL
    url = f"{airbyte_url}/connections"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request params
    params_data = {}

    # Wrap the request body
    json_payload = {}

    # Make the DELETE request
    logger.debug(f'+++: GET {url} \n Param {params_data} \n Header {header_data} \n Payload {json_payload}')
    response = requests.get(url, params=params_data, headers=header_data, json=json_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    if response.status_code == 200: # successful
        response_data = response.json()
        # handle empty list
        if 'data' in response_data:
            return_value = response_data['data']
            return return_value
    else:
        logger.error(f'{response.status_code} - {response.text}')
    
    return_value = []
    return return_value