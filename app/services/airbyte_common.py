import requests
from logzero import logger

from config.config import get_app_config
settings = get_app_config()

AIRBYTE_BASEURL = f"{settings.AIRBYTE_API_URL}/{settings.AIRBYTE_API_VERSION}"

def get_all_stream(source_id: str, destination_id: str) -> dict:
    """_summary_

    Args:
        source_id (str)
        destination_id (str)

    Returns:
        dict: 
        { 'streams': [
                {
                    "syncMode": "full_refresh_overwrite",
                    "name": "cars"
                },
                {
                    "name": "_airbyte_raw_cars",
                    "syncMode": "full_refresh_overwrite"
                }
            ]
        }
    """
    stream_list = []
    
    # Construct the API URL
    airbyte_url = AIRBYTE_BASEURL
    url = f"{airbyte_url}/streams"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    # Wrap the request body
    param_payload = {
        'sourceId': source_id,
        'destinationId': destination_id,
        'ignoreCache': 'true'
    }

    # Make the GET request
    logger.debug(f'+++: Calling {url} - Header {header_data} - Payload {param_payload}')
    response = requests.get(url, headers=header_data, params=param_payload)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    
    return_value = {}
    if response.status_code == 200: # successful
        response_data = response.json() # list
        for streams in response_data:
            for key in streams.keys():
                if key == 'streamName':
                    stream_info = {
                        "syncMode": "full_refresh_overwrite",
                        "name": streams[key]
                    }
                    stream_list.append(stream_info)
        data = {
            'streams': stream_list
        }
        return_value = {
            'status': 'ok',
            'data': data,
            'message': 'create successfully'
        }
    else:
        logger.error(f'{response.status_code} - {response.text}')
        return_value = {
            'status': 'bad',
            'message': response.text
        }
        
    return return_value

def get_id_by_name(resource_name: str, data_type: str) -> str:
    """_summary_

    Args:
        resource_name (str): name of datasource to lookup
        data_type (str): either "sources, destinations, workspaces or connections"

    Returns:
        str: the uuid of the resource
    """
    mapping_dict = {
        'sources': {
            'key': 'sourceId'
        },
        'destinations': {
            'key': 'destinationId'
        },
        'connections': {
            'key': 'sourceId'
        },
        'workspaces': {
            'key': 'workspaceId'
        }
    }
    # Construct the API URL
    airbyte_url = AIRBYTE_BASEURL
    url = f"{airbyte_url}/{data_type}"

    # Wrap the request header
    header_data = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }

    param_payload = {}
    # Make the GET request
    logger.debug(f'+++: Get UUID {url} - Header {header_data}')
    response = requests.get(url, headers=header_data)
    logger.debug(f'+++: {response.status_code} - {response.text}')
    
    return_value = ''
    if response.status_code == 200: # successful
        response_data = response.json()
        if 'data' in response_data:
            for component in response_data['data']:
                # Return workspace_id immediately
                if data_type == 'workspaces':
                    return component[mapping_dict[data_type]['key']]
                
                if component['name'] == resource_name:
                    return_value = component[mapping_dict[data_type]['key']]

    return return_value
